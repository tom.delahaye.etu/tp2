class Component {
	tagName;
	children;
	constructor(tagName, children) {
		this.tagName = tagName;
		this.children = children;
	}
	render() {
		if (this.children != null)
			return `<${this.tagName}>${this.children}</${this.tagName}>`;
		return `<${this.tagName} />`;
	}
}

export default Component;
